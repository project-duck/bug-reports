### Summary

(Summarize the bug encountered concisely)

### PvE or PvP?

Check all that apply (check by placing an x in the brackets, like: `[x]`)

- [ ] PvE
- [ ] PvP

### What class?

(What class does this affect. Please specify the lowest class that this applies to, e.g. if a bug affects Gladiator, does it affect Swordsman or Warrior?)

### What skill(s)

- (List the skill name(s) or even better add the skill ID. You can get the skill ID from Minerva https://reference.fatduckdn.com/skills )


### Steps to reproduce

1. (How one can reproduce the issue - this is very important)
2. (Videos and screenshots are great too)


### What is the current bug behavior?

(What actually happens)


### What is the expected correct behavior?

(What you should see instead)


/label ~Bug ~Reproduced ~"Needs Investigation" ~Skill
