### Summary

(Summarize the bug encountered concisely)


### What item(s)

- (List the item name(s) or even better add the item ID. You can get the item ID from Minerva https://reference.fatduckdn.com/items/search )


### Steps to reproduce

1. (How one can reproduce the issue - this is very important)
2. (Videos and screenshots are great too)


### What is the current bug behavior?

(What actually happens)


### What is the expected correct behavior?

(What you should see instead)


/label ~Bug ~Reproduced ~"Needs Investigation" ~Item
