### Summary

(Summarize the bug encountered concisely. If its an MID, provide the MID and where you found it. Screenshots are okay but PLEASE write out the MID)


### (Optional) Provide new text

(Suggest a new text for this message)

/label ~Bug ~Reproduced ~"Needs Investigation" ~UiString
