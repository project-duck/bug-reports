# Bug Reports

Place for bug reports.

 - [Create a new bug report](https://gitlab.com/project-duck/bug-reports/issues/new)
 - [List bug reports](https://gitlab.com/project-duck/bug-reports/issues)

## How to create a good bug report

Provide us as much information as possible to help us understand your issue. 
When creating an issue, please use one of the pregenerated templates and fill 
out the sections as directed.

![Imgur](https://i.imgur.com/OR0wO2A.png)

We will triage your bug report according to its severity and will track updates on it.

Thank you for helping us!

### Security Issues

If you find a security issue, please use the "Confidential" option when creating 
your bug report, or DM Vahr on Discord directly.

![Imgur](https://i.imgur.com/MmrWilM.png)
